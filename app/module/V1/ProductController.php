<?php declare(strict_types = 1);

namespace App\Module\V1;

use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\OpenApi;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Annotation\Controller\RequestParameter;
use Apitte\Core\Annotation\Controller\RequestParameters;
use Apitte\Core\Annotation\Controller\Tag;
use Apitte\Core\Exception\Api\ClientErrorException;
use Apitte\Core\Exception\Api\ServerErrorException;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use App\Domain\Api\Facade\ProductFacade;
use App\Domain\Api\Request\ProductReqDto;
use App\Domain\Api\Response\ProductResDto;
use App\Model\Exception\Runtime\Database\EntityNotFoundException;
use Doctrine\DBAL\Exception\DriverException;
use Nette\Http\IResponse;

/**
 * @Path("/products")
 * @Tag("Products")
 */
class ProductController extends BaseV1Controller
{

	private ProductFacade $productFacade;

	public function __construct(ProductFacade $productFacade)
	{
		$this->productFacade = $productFacade;
	}

	/**
	 * @OpenApi("
	 *   summary: List products.
	 * ")
	 * @Path("/")
	 * @Method("GET")
	 * @RequestParameters({
	 * 		@RequestParameter(name="limit", type="int", in="query", required=false, description="Data limit"),
	 * 		@RequestParameter(name="offset", type="int", in="query", required=false, description="Data offset")
	 * })
	 * @return ProductResDto[]
	 */
	public function index(ApiRequest $request): array
	{
		return $this->productFacade->findAll(
			intval($request->getParameter('limit', 10)),
			intval($request->getParameter('offset', 0))
		);
	}

	/**
	 * @OpenApi("
	 *   summary: Create new product.
	 * ")
	 * @Path("/create")
	 * @Method("POST")
	 * @Tag(name="request.dto", value="App\Domain\Api\Request\ProductReqDto")
	 */
	public function create(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		/** @var ProductReqDto $dto */
		$dto = $request->getParsedBody();

		try {
			$this->productFacade->create($dto);

			return $response->withStatus(IResponse::S200_OK)
				->withHeader('Content-Type', 'application/json');
		} catch (DriverException $e) {
			throw ServerErrorException::create()
				->withMessage('Cannot create product')
				->withPrevious($e);
		}
	}

	/**
	 * @OpenApi("
	 *   summary: Update product.
	 * ")
	 * @Path("/{id}")
	 * @Method("PUT")
	 * @Tag(name="request.dto", value="App\Domain\Api\Request\ProductReqDto")
     * @RequestParameters({
     *      @RequestParameter(name="id", in="path", type="int", description="Product ID"),
     * })
	 */
	public function update(ApiRequest $request, ApiResponse $response): ApiResponse
	{
		/** @var ProductReqDto $dto */
		$dto = $request->getParsedBody();

		try {
			$this->productFacade->update(intval($request->getParameter('id')), $dto);

			return $response->withStatus(IResponse::S200_OK)
				->withHeader('Content-Type', 'application/json');
		} catch (DriverException $e) {
			throw ServerErrorException::create()
				->withMessage('Cannot update product')
				->withPrevious($e);
		}
	}

    /**
     * @OpenApi("
     *   summary: Delete product.
     * ")
     * @Path("/{id}")
     * @Method("DELETE")
     * @RequestParameters({
     *      @RequestParameter(name="id", in="path", type="int", description="Product ID")
     * })
     */
    public function delete(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        try {
            $this->productFacade->delete(intval($request->getParameter('id')));

            return $response->withStatus(IResponse::S200_OK)
                ->withHeader('Content-Type', 'application/json');
        } catch (DriverException $e) {
            throw ServerErrorException::create()
                ->withMessage('Cannot delete product')
                ->withPrevious($e);
        }
    }

	/**
	 * @OpenApi("
	 *   summary: Get product by id.
	 * ")
	 * @Path("/{id}")
	 * @Method("GET")
	 * @RequestParameters({
	 *      @RequestParameter(name="id", in="path", type="int", description="Product ID")
	 * })
	 */
	public function byId(ApiRequest $request): ProductResDto
	{
		try {
			return $this->productFacade->findOne(intval($request->getParameter('id')));
		} catch (EntityNotFoundException $e) {
			throw ClientErrorException::create()
				->withMessage('Product not found')
				->withCode(IResponse::S404_NOT_FOUND);
		}
	}

}
