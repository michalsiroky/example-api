<?php declare(strict_types = 1);

namespace App\Domain\Api\Facade;

use App\Domain\Api\Request\ProductReqDto;
use App\Domain\Api\Response\ProductResDto;
use App\Domain\Api\Response\UserResDto;
use App\Model\Database\Entity\Product;
use App\Model\Database\EntityManager;
use App\Model\Exception\Runtime\Database\EntityNotFoundException;

final class ProductFacade
{

	private EntityManager $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * @param mixed[] $criteria
	 * @param string[] $orderBy
	 * @return UserResDto[]
	 */
	public function findBy(array $criteria = [], array $orderBy = ['id' => 'ASC'], int $limit = 10, int $offset = 0): array
	{
		$entities = $this->em->getProductRepository()->findBy($criteria, $orderBy, $limit, $offset);
		$result = [];

		foreach ($entities as $entity) {
			$result[] = ProductResDto::from($entity);
		}

		return $result;
	}

	/**
	 * @return UserResDto[]
	 */
	public function findAll(int $limit = 10, int $offset = 0): array
	{
		return $this->findBy([], ['id' => 'ASC'], $limit, $offset);
	}

	/**
	 * @param mixed[] $criteria
	 * @param string[] $orderBy
	 */
	public function findOneBy(array $criteria, ?array $orderBy = null): ProductResDto
	{
		$entity = $this->em->getProductRepository()->findOneBy($criteria, $orderBy);

		if (!$entity) {
			throw new EntityNotFoundException();
		}

		return ProductResDto::from($entity);
	}

	public function findOne(int $id): ProductResDto
	{
		return $this->findOneBy(['id' => $id]);
	}

	public function create(ProductReqDto $dto): Product
	{
		$product = new Product(
			$dto->name,
			$dto->price
		);

		$this->em->persist($product);
		$this->em->flush($product);

		return $product;
	}

	public function update(int $id, ProductReqDto $dto): Product
	{
		$product = $this->em->getProductRepository()->find($id);

		if (!$product) {
			throw new EntityNotFoundException();
		}

		$product->setName($dto->name);
		$product->setPrice($dto->price);

		$this->em->persist($product);
		$this->em->flush($product);

		return $product;
	}

	public function delete(int $id): void
	{
		$product = $this->em->getProductRepository()->find($id);

		if (!$product) {
			throw new EntityNotFoundException();
		}

		$this->em->remove($product);
		$this->em->flush();
	}

}
