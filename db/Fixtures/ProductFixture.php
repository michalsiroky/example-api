<?php declare(strict_types = 1);

namespace Database\Fixtures;

use App\Model\Database\Entity\Product;
use App\Model\Fixtures\ReflectionLoader;
use Doctrine\Persistence\ObjectManager;

class ProductFixture extends AbstractFixture
{

	private ObjectManager $manager;

	public function load(ObjectManager $manager): void
	{
		$this->manager = $manager;

		foreach ($this->getRandomProducts() as $product) {
			$this->manager->persist($product);
		}

		$this->manager->flush();
	}

	/**
	 * @return Product[]
	 */
	protected function getRandomProducts(): iterable
	{
		$loader = new ReflectionLoader();
		$objectSet = $loader->loadData([
			Product::class => [
				'product{1..100}' => [
					'__construct' => [
						'Product <current()>',
						'<numberBetween(1, 50000)>'
					],
					'id' => '<current()>',
				],
			],
		]);

		return $objectSet->getObjects();
	}

}
